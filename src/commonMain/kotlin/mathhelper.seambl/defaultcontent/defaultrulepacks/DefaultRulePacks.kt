package mathhelper.seambl.defaultcontent.defaultrulepacks


val defaultRulePacks = defaultStandardMathRulePacks + defaultCombinatoricsRulePacks + defaultComplexRulePacks + defaultLogicRulePacks + defaultPhysicsRulePacks

val defaultRulePacksMap = defaultRulePacks.associateBy { it.code!! }
