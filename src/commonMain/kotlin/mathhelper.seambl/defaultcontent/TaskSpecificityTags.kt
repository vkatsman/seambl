package mathhelper.seambl.defaultcontent


enum class TaskTagCode(val code: String,
                       val nameEn: String,
                       val nameRu: String,
                       val descriptionEn: String = nameEn,
                       val descriptionRu: String = nameRu) {
    PROOF("proof", "Proof", "Доказательство"),
    SIMPLIFICATION("simplification", "Simplification", "Упрощение"),
    COMPUTATION("computation", "Computation", "Вычисление"),
    EQUATION("equation", "Equation", "Уравнение"),

    FACTORIZATION("factorization", "Factorization", "Разложение на множители"),
    REDUCE("reduce", "Factorization", "Разложение на множители"),

    FORMULA_BASE("formulaBase", "Formula Application Task", "Задача на применение формулы"),
    FORMULA_DEDUCE("formulaDeduce", "Formula Deducing Task", "Задача, в которой выводится формула"),
    TRICK("trick", "Task with Trick", "Задача с изюминкой"),


    // solution patterns
    SHORT_MULTIPLICATION("shortMultiplication", "Short Multiplication", "Сокращенное умножение"),
    SUM_SQRS("sumSqrs", "Sum of Squares", "Сумма квадратов"),
    DIFF_SQRS("diffSqrs", "Difference of Squares", "Разность квадратов"),
    SQR_SUM("sqrSum", "Square of Sum", "Квадрат суммы"),
    SQR_DIFF("sqrDiff", "Square of Difference", "Квадрат разности"),
    SUM_CUBES("sumCubes", "Sum of Cubes", "Сумма кубов"),
    DIFF_CUBES("diffCubes", "Difference of Cubes", "Разность кубов"),
    CUBE_SUM("cubeSum", "Cube of Sum", "Куб суммы"),
    CUBE_DIFF("cubeDiff", "Cube of Difference", "Куб разности"),

    FRACTION("fraction", "Fraction", "Дробь"),

    TRIGONOMETRY("trigonometry", "Trigonometry", "Тригонометрия"),
    INVERSE_TRIGONOMETRY("inverseTrigonometry", "Inverse Trigonometry Functions", "Обратные тригонометрические функции"),
    DEGREES("degrees", "Degrees of Angles", "Градусные муры углов"),
    PYTHAGOREAN_IDENTITY("pythagoreanIdentity", "Pythagorean Trigonometric Identity", "Основное тригонометрическое тождество"),

    TRIGONOMETRY_ANGLE_SUM("trigonometryAngleSum", "Expanding of Sum or Difference of Angle in Trigonometry Function", "Раскрытие тригонометрических функций от суммы и разности"),
    TRIGONOMETRY_SUM("trigonometrySum", "Expanding of Sum or Difference of Trigonometry Functions", "Раскрытие суммы или разности тригонометрических функций"),
    TRIGONOMETRY_PRODUCT("trigonometryProduct", "Expanding of Product of Trigonometry Functions", "Раскрытие произведения тригонометрических функций"),
    TRIGONOMETRY_REFLECTIONS("trigonometryReflections", "Trigonometry Reflection Formulas", "Формулы приведения в тригонометрии"),

    LOGARITHM ("logarithm", "Logarithm", "Логарифм")
}