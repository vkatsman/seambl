package mathhelper.seambl.visualization

import mathhelper.seambl.expressiontree.ExpressionSubstitution
import mathhelper.seambl.factstransformations.ComparableTransformationsPart
import mathhelper.seambl.factstransformations.FactSubstitution
import mathhelper.seambl.factstransformations.MainChain

fun transformationPartsToLog (transformationsPart: ComparableTransformationsPart): String{
    return transformationsPart.computeIdentifier(false)
}

fun transformationPartsToLog (transformation: ExpressionSubstitution): String{
    return transformation.computeIdentifier(false)
}

fun transformationPartsToLog (transformation: FactSubstitution): String{
    return transformation.computeIdentifier(false)
}

fun transformationPartsToLog (transformation: MainChain): String{
    return transformation.computeIdentifier(false)
}


