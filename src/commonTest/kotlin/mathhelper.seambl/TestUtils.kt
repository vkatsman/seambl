package mathhelper.seambl

import kotlin.test.assertTrue

/**
 * Throws an [AssertionError] calculated by [lazyMessage] if the [value] is false
 * and runtime assertions have been enabled on the JVM using the *-ea* JVM option.
 */
inline fun assert(value: Boolean, lazyMessage: () -> Any) {
    if (!value) {
        val assertAction = lazyMessage.invoke()
        if (assertAction is String) {
            assertTrue(value, assertAction)
        }
    }
}

inline fun assert(value: Boolean) {
    if (!value) {
        assertTrue(value)
    }
}