package mathhelper.seambl.tasksolutions

import mathhelper.seambl.api.*
import mathhelper.seambl.assert
import mathhelper.seambl.config.CompiledConfiguration
import mathhelper.seambl.config.FactTransformationRule
import mathhelper.seambl.config.FunctionConfiguration
import mathhelper.seambl.config.FunctionIdentifier
import mathhelper.seambl.factstransformations.FactConstructorViewer
import mathhelper.seambl.factstransformations.FactSubstitution
import mathhelper.seambl.factstransformations.MainLineAndNode
import mathhelper.seambl.factstransformations.parseFromFactIdentifier
import mathhelper.seambl.logs.log
import mathhelper.seambl.mainpoints.checkFactsInMathML
import mathhelper.seambl.mainpoints.compiledConfigurationBySettings
import mathhelper.seambl.mainpoints.configSeparator
import kotlin.test.assertEquals
import kotlin.test.Ignore
import kotlin.test.Test

class SetTheory {
    val wellKnownFunctions = listOf(
            FunctionIdentifier("", 0),
            FunctionIdentifier("", 1)
    )

    val wellKnownFunctionsString = "" +
            "${configSeparator}0"

    val expressionTransformationRules = listOf(
            expressionSubstitutionFromStructureStrings("(or(A;and(B;C)))", "(and(or(A;B);or(A;C)))"),
            expressionSubstitutionFromStructureStrings("(or(and(B;C);A))", "(and(or(B;A);or(C;A)))"),
            expressionSubstitutionFromStructureStrings("(and(A;or(B;C)))", "(or(and(A;B);and(A;C)))"),
            expressionSubstitutionFromStructureStrings("(and(or(B;C);A))", "(or(and(B;A);and(C;A)))"),

            expressionSubstitutionFromStructureStrings("(not(not(A)))", "(A)"),
            expressionSubstitutionFromStructureStrings("(or(A;A))", "(A)"),
            expressionSubstitutionFromStructureStrings("(and(A;A))", "(A)"),

            expressionSubstitutionFromStructureStrings("(not(and(A;B)))","(or(not(A);not(B)))"),
            expressionSubstitutionFromStructureStrings("(not(or(A;B)))","(and(not(A);not(B)))"),

            expressionSubstitutionFromStructureStrings("(or(not(A);B))","(implic(A;B))"),
            expressionSubstitutionFromStructureStrings("(or(B;not(A)))","(implic(A;B))"),
            expressionSubstitutionFromStructureStrings("(and(A;not(B)))","(set-(A;B))"),
            expressionSubstitutionFromStructureStrings("(and(not(B);A))","(set-(A;B))"),
            expressionSubstitutionFromStructureStrings("(set-(A;B))","(not(implic(A;B)))"),
            expressionSubstitutionFromStructureStrings("(implic(A;B))","(not(set-(A;B)))"),

            expressionSubstitutionFromStructureStrings("(or(A;not(A)))","(1)"),
            expressionSubstitutionFromStructureStrings("(or(not(A);A))","(1)"),
            expressionSubstitutionFromStructureStrings("(and(A;not(A)))","(0)"),
            expressionSubstitutionFromStructureStrings("(and(not(A);A))","(0)"),

            expressionSubstitutionFromStructureStrings("(or(A;1))","(1)"),
            expressionSubstitutionFromStructureStrings("(or(1;A))","(1)"),
            expressionSubstitutionFromStructureStrings("(and(A;1))","(A)"),
            expressionSubstitutionFromStructureStrings("(and(1;A))","(A)"),
            expressionSubstitutionFromStructureStrings("(or(A;0))","(A)"),
            expressionSubstitutionFromStructureStrings("(or(0;A))","(A)"),
            expressionSubstitutionFromStructureStrings("(and(A;0))","(0)"),
            expressionSubstitutionFromStructureStrings("(and(0;A))","(0)"),

            expressionSubstitutionFromStructureStrings("(set-(A;0))","(A)"),
            expressionSubstitutionFromStructureStrings("(set-(A;1))","(0)"),
            expressionSubstitutionFromStructureStrings("(set-(0;A))","(0)"),
            expressionSubstitutionFromStructureStrings("(set-(1;A))","(not(A))"),

            expressionSubstitutionFromStructureStrings("(implic(A;0))","(not(A))"),
            expressionSubstitutionFromStructureStrings("(implic(A;1))","(1)"),
            expressionSubstitutionFromStructureStrings("(implic(0;A))","(1)"),
            expressionSubstitutionFromStructureStrings("(implic(1;A))","(A)"),

            expressionSubstitutionFromStructureStrings("(not(0))","(1)"),
            expressionSubstitutionFromStructureStrings("(not(1))","(0)")
    )

    val expressionTransformationRulesString = "" +
            "A\\/(A/\\B)${configSeparator}A${configSeparator}" +
            "A/\\(A\\/B)${configSeparator}A${configSeparator}" +
            "A\\/A${configSeparator}A${configSeparator}" +
            "A/\\A${configSeparator}A${configSeparator}" +
            "A\\/(B\\/C)${configSeparator}A\\/B\\/C${configSeparator}" +
            "A/\\(B/\\C)${configSeparator}A/\\B/\\C${configSeparator}" +
            "A\\/(B/\\C)${configSeparator}(A\\/B)/\\(A\\/C)${configSeparator}" +
            "A/\\(B\\/C)${configSeparator}(A/\\B)\\/(A/\\C)${configSeparator}" +
            "!(A/\\B)${configSeparator}!A\\/!B${configSeparator}" +
            "!(A\\/B)${configSeparator}!A/\\!B${configSeparator}" +
            "A->B${configSeparator}!A\\/B${configSeparator}" +
            "A\\B${configSeparator}A/\\!B${configSeparator}" +

            "A\\/B${configSeparator}B\\/A${configSeparator}" +
            "A/\\B${configSeparator}B/\\A${configSeparator}" +

            "!(!(A))${configSeparator}A"


    @Test
    fun asIsWithoutSubstitutions() {
        val result = compareWithoutSubstitutions(
                "!(C/\\A)/\\(B\\/!C)",
                "!(A/\\C)/\\(B\\/!C)",
                "setTheory"
        )
        assertEquals(true, result)
    }

    @Test
    fun asIsWithoutSubstitutionsInternalBrackets() {
        val result = compareWithoutSubstitutions(
                "not(or(or(B,A),C))",
                "not(or(A,B,C))",
                "setTheory"
        )
        assertEquals(true, result)
    }

    @Test
    fun substitutionApplicationTest (){
        val places = findSubstitutionPlacesCoordinatesInExpressionJSON (
                "A|A",
                "A",
                "B",
                scope = "setTheory",
                basedOnTaskContext = true
        )
        assertEquals("{\"substitutionPlaces\":[{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"3\",\"startPosition\":\"0\",\"endPosition\":\"1\"},{\"parentStartPosition\":\"0\",\"parentEndPosition\":\"3\",\"startPosition\":\"2\",\"endPosition\":\"3\"}]}", places)
    }

    @Test
    fun wrongWithoutSubstitutions() {
        val result = compareWithoutSubstitutions(
                "!(B/\\A)/\\(B\\/!C)",
                "!(A/\\C)/\\(B\\/!C)",
                "setTheory",
                maxExpressionBustCount = 2
        )
        assertEquals(false, result)
    }

    @Test
    fun LongConjunction() {
        val result = compareWithoutSubstitutions(
                "C/\\A/\\B/\\D",
                "C/\\A/\\(B/\\D)",
                "setTheory"
        )
        assertEquals(true, result)
    }

    @Test
    fun LongConjunctionWithPermutation() {
        val result = compareWithoutSubstitutions(
                "(C/\\B)/\\A/\\D",
                "D/\\(A/\\(B/\\C))",
                "setTheory"
        )
        assertEquals(true, result)
    }

    @Test
    @Ignore
    fun proveTaskSet1() {
        val result = checkFactsInMathML(
                brushedMathML = "(!A/\\B)\\/C=(!A\\/!C)/\\(B\\/!C)=!(C/\\A)/\\(B\\/!C)",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(or(and(not(A);B);C))}{=}{(and(not(and(C;A));or(B;not(C))))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "",
                scopeFilter = ";;;setTheory"
        )

        val logRef = log.getLogInPlainText()

        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
//        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>4</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>3</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>+</mo><mn>4</mn><mo>*</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mo>+</mo><mn>2</mn><mo>*</mo><mi>c</mi><mi>o</mi><msup><mi>s</mi><mn>2</mn></msup><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>(</mo><mn>1</mn><mo>+</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mn>2</mn><mo>*</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>2</mn><mo>(</mo><mn>2</mn><mo>*</mo><msup><mi>cos</mi><mn>2</mn></msup><mo>(</mo><mi>x</mi><mo>)</mo><msup><mo>)</mo><mn>2</mn></msup><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mn>8</mn><mo>*</mo><mo>(</mo><mi>c</mi><mi>o</mi><mi>s</mi><mo>(</mo><mi>x</mi><mrow><mo>)</mo><msup><mo>)</mo><mn>4</mn></msup></mrow></math>", result)
    }

    @Test
    fun proveTaskSet2() {
        val result = checkFactsInMathML(
                brushedMathML = "(!A/\\B)\\/C=(!A\\/!C)/\\(B)=!(C/\\A)/\\(B\\/!C)",
                wellKnownFunctions = wellKnownFunctionsString,
                expressionTransformationRules = expressionTransformationRulesString,
                targetFactIdentifier = "EXPRESSION_COMPARISON{(or(and(not(A);B);C))}{=}{(and(not(and(C;A));or(B;not(C))))}",
                targetVariablesNames = "",
                additionalFactsIdentifiers = "",
                scopeFilter = ";;;setTheory"
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
    }

    @Test
    fun proveTaskTexSimpleWrongNotObvious() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\neg A\\wedge \\neg B\\wedge \\neg C=\\neg \\left(A\\vee B\\vee C\\right)",
                wellKnownFunctions = wellKnownFunctions,
                expressionTransformationRules = expressionTransformationRules,
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(and(not(A);not(B);not(C)))' and '(not(or(A;B;C)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{red}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSimpleFull() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\neg A\\wedge \\neg B\\wedge \\neg C=\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\right)\\wedge \\neg C=\\neg (\\left(A\\vee B\\right)\\vee C)=\\neg \\left(A\\vee B\\vee C\\right)",
                wellKnownFunctions = wellKnownFunctions,
                expressionTransformationRules = expressionTransformationRules,
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg \\left(A\\vee B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg (\\left(A\\vee B\\right)\\vee C)\\textcolor{green}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSimple() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\neg A\\wedge \\neg B\\wedge \\neg C=\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\vee C\\right)",
                rulePacks = listOf("Logic").toTypedArray(),
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))",
                additionalFactsIdentifiers = ""
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg \\left(A\\vee B\\right)\\wedge \\neg C\\textcolor{green}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSimpleUnclearTransformations() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\neg A\\wedge \\neg B\\wedge \\neg C=\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C=\\neg \\left(A\\vee B\\vee C\\right)",
                wellKnownFunctions = wellKnownFunctions,
                rulePacks = listOf("Logic").toTypedArray(),
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))"
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(and(and(not(A);not(B));not(C)))' and '(not(or(A;B;C)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{red}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSimpleUnclearTransformationsColored() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{red}{=}\\neg} \\left(A\\vee B\\vee C\\right)",
                wellKnownFunctions = wellKnownFunctions,
                rulePacks = listOf("Logic").toTypedArray(),
                startExpressionIdentifier = "(and(not(A);not(B);not(C)))",
                endExpressionIdentifier = "(not(or(A;B;C)))"
        )

        val logRef = log.getLogInPlainText()


        assertEquals("Error: Unclear transformation between '(and(and(not(A);not(B));not(C)))' and '(not(or(A;B;C)))' ", result.errorMessage)
        assertEquals("\\textcolor{purple}{\\neg A\\wedge \\neg B\\wedge \\neg C\\textcolor{green}{=}\\left(\\neg A\\wedge \\neg B\\right)\\wedge \\neg C\\textcolor{red}{=}\\neg \\left(A\\vee B\\vee C\\right)}",
                result.validatedSolution)
    }

    @Test
    fun proveTaskTexSolutionWithReductionRules() {
        val result = checkSolutionInTex(
                originalTexSolution = "\\textcolor{purple}{a\\wedge \\left(a\\vee b\\right)\\textcolor{green}{=}\\left(a\\vee 0\\right)\\wedge \\left(a\\vee b\\right)\\textcolor{red}{=}a\\vee \\left(0\\wedge b\\right)=a\\vee 0=a}",
                rulePacks = listOf("Logic").toTypedArray(),
                startExpressionIdentifier = "(and(a;or(a;b)))",
                endExpressionIdentifier = "(a)"
        )

        val logRef = log.getLogInPlainText()


        assertEquals("", result.errorMessage)
        assertEquals("\\textcolor{purple}{a\\wedge \\left(a\\vee b\\right)\\textcolor{green}{=}\\left(a\\vee 0\\right)\\wedge \\left(a\\vee b\\right)\\textcolor{green}{=}a\\vee \\left(0\\wedge b\\right)\\textcolor{green}{=}a\\vee 0\\textcolor{green}{=}a}",
                result.validatedSolution)
    }

    @Test
    fun correctCustomTransformations() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>&#xAC;</mo><mo>&#xAC;</mo><mi>b</mi><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">=</mo><mi>b</mi></math>",
                wellKnownFunctions = "",
                expressionTransformationRules = " ",
                targetFactIdentifier = "",
                targetVariablesNames = "",
                minNumberOfMultipliersInAnswer = "",
                additionalFactsIdentifiers = "",
                makeFirstSingleTransformationChainFactCorrectWithoutAdditionalFacts = "1"
        )

        val logRef = log.getLogInPlainText()

        assert(!result.contains("Error"))
        assert(!result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>&#xAC;</mo><mo>&#xAC;</mo><mi>b</mi><mrow mathvariant=\"bold\" mathcolor=\"#007F00\"><mo>=</mo></mrow><mi>b</mi></math>",
                result)
    }

    @Test
    fun wrongCustomTransformations() {
        val result = checkFactsInMathML(
                brushedMathML = "<math mathcolor=\"#7F00FF\" xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>&#xAC;</mo><mo>&#xAC;</mo><mi>b</mi><mo mathvariant=\"bold\" mathcolor=\"#FF0000\">=</mo><mi>a</mi></math>",
                wellKnownFunctions = "",
                expressionTransformationRules = " ",
                targetFactIdentifier = "",
                targetVariablesNames = "",
                minNumberOfMultipliersInAnswer = "",
                additionalFactsIdentifiers = "",
                makeFirstSingleTransformationChainFactCorrectWithoutAdditionalFacts = "1"
        )

        val logRef = log.getLogInPlainText()

        assert(result.contains("Error"))
        assert(result.contains("#FF"))
        assertEquals("<math xmlns=\"http://www.w3.org/1998/Math/MathML\" mathcolor=\"#7F00FF\"><mo>&#xAC;</mo><mo>&#xAC;</mo><mi>b</mi><mrow mathvariant=\"bold\" mathcolor=\"#FF0000\"><mo>=</mo></mrow><mi>a</mi><mspace linebreak=\"newline\"/><mtext mathvariant=\"bold\" mathcolor=\"#FF0000\">Error: Unclear transformation between '(not(not(b)))' and '(a)' </mtext></math>",
                result)
    }

    @Test
    @Ignore
    fun oldIdentifierToNew() {
        val functionConfiguration = FunctionConfiguration(setOf("", "setTheory"))
        val factConstructorViewer: FactConstructorViewer = FactConstructorViewer(compiledConfiguration = CompiledConfiguration(functionConfiguration = functionConfiguration))
        val fact = parseFromFactIdentifier("(!A/\\B)\\/C;ec;=;ec;!(C/\\A)/\\(B\\/!C)", functionConfiguration = functionConfiguration)!!
        val newIdentifier = factConstructorViewer.constructIdentifierByFact(fact)
        print(newIdentifier)
    }
}