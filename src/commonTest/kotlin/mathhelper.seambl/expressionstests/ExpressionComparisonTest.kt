package mathhelper.seambl.expressionstests

import mathhelper.seambl.api.compareWithoutSubstitutions
import mathhelper.seambl.config.ComparisonType
import mathhelper.seambl.config.CompiledConfiguration
import kotlin.test.Ignore
import kotlin.test.Test
import mathhelper.seambl.substitutiontests.parseStringExpression
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ExpressionComparisonTest {
    // This test does not work and difference between left and right is always 2 * PI * i or more
    @Test
    @Ignore
    fun testSumOfThreeLogsWithEmptyDomain() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val result = compareWithoutSubstitutions(
                "ln(-sqrt(x)) + ln(-y^2) + ln(-z^2)",
                "ln((y * z)^2 * (-sqrt(x)))",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }

    @Test
    @Ignore //TODO solve problem with n-placement functions
    fun testFactorialExpansion() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.EQUAL
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val result = compareWithoutSubstitutions(
                "m!/(m-n)!",
                "m*(m-1)!/(m-n)!",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, result)
    }
}
