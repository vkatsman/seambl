package mathhelper.seambl.expressionstests

import mathhelper.seambl.api.compareWithoutSubstitutions
import mathhelper.seambl.config.ComparisonType
import mathhelper.seambl.config.CompiledConfiguration
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class ExpressionInequalityComparisonTest {
    @Test
    fun testSimpleLessThenTest() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val res = compareWithoutSubstitutions(
                "x",
                "x + 1",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(true, res);

    }

    @Test
    fun testLessSumm3Vars() {
        val compiledConfiguration = CompiledConfiguration()
        compiledConfiguration.comparisonSettings.defaultComparisonType = ComparisonType.LEFT_LESS
        compiledConfiguration.comparisonSettings.useOldSimpleProbabilityTesting = false
        val res = compareWithoutSubstitutions(
                "x + y + z",
                "3.14",
                compiledConfiguration = compiledConfiguration
        )
        assertEquals(false, res);
    }

}