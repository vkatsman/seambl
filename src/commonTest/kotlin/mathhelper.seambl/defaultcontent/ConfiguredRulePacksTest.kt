package mathhelper.seambl.defaultcontent

import mathhelper.seambl.api.compareWithoutSubstitutions
import mathhelper.seambl.api.getAllExtendedAlgebraSubstitutions
import mathhelper.seambl.api.getAllSubstitutions
import mathhelper.seambl.api.structureStringToExpression
import mathhelper.seambl.expressiontree.NodeType
import kotlin.test.Test
import kotlin.test.assertTrue
import mathhelper.seambl.assert

class ConfiguredRulePacksTest {
    @Test
    fun testAllExtendedAlgebraSubstitutionsCorrectness() {
        val allRules = getAllExtendedAlgebraSubstitutions()
        var countOfInEqualRules = 0
        for (rule in allRules) {
            assert(rule.left.value == "",
                    { "Error: rule.left.value is not empty top in rule '${rule.code}' '${rule.left}' ->  '${rule.right}' " })
            assert(rule.right.value == "",
                    { "Error: rule.right.value is not empty top in rule '${rule.code}' '${rule.left}' ->  '${rule.right}' " })

            if (rule.left.nodeType != NodeType.FUNCTION || rule.right.nodeType != NodeType.FUNCTION) {
                assert(rule.code.isNotBlank(),
                        { "Error: rule.code is blank in rule '${rule.code}' '${rule.left}' ->  '${rule.right}' " })
            } else {
                if (!compareWithoutSubstitutions(rule.left, rule.right, notChangesOnVariablesFunction = setOf())) {
                    countOfInEqualRules ++
                    println("Warning: '${rule.left}' != '${rule.right}'")
                }
            }
        }
        println(countOfInEqualRules)
        assert(countOfInEqualRules <= 50, { "too high countOfInEqualRules = '$countOfInEqualRules" })
    }
}