import com.google.gson.GsonBuilder
import mathhelper.seambl.config.RulePackITR
import mathhelper.seambl.config.TaskSetITR
import mathhelper.seambl.defaultcontent.defaultrulepacks.defaultRulePacks
import mathhelper.seambl.defaultcontent.toRulePackFrontInput
import mathhelper.seambl.defaultcontent.toTaskSetFrontInput
import kotlin.test.Ignore
import java.io.FileDescriptor
import java.io.FileWriter

fun getGsonWriter() = GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create()


fun printJsonRulePacksInFrontInput(namespace: String, outputDirectoryPath: String, rulePacks: List<RulePackITR> = defaultRulePacks) {
    val gsonWriter = getGsonWriter()
    var counter = 10
    for (rulePack in rulePacks) {
        val modifiedRulePack = rulePack.clone(namespaceCode = namespace).toRulePackFrontInput()

        val writer = FileWriter(outputDirectoryPath + counter.toString() + "__" + rulePack.code + ".json")
        gsonWriter.toJson(modifiedRulePack, writer)
        writer.flush()
        writer.close()
        counter += 1
    }
}


fun printJsonTaskSetsInFrontInput(namespace: String, outputDirectoryPath: String, taskSets: List<TaskSetITR>) {
    val gsonWriter = getGsonWriter()
    for (taskSet in taskSets) {
        val modifiedTaskSet = taskSet.cloneBesidesMaps(namespaceCode = namespace).toTaskSetFrontInput()

        val writer = FileWriter(outputDirectoryPath + taskSet.code + ".json")
        gsonWriter.toJson(modifiedTaskSet, writer)
        writer.flush()
        writer.close()
    }
}


fun printJsonTaskSets(namespace: String, outputDirectoryPath: String, taskSets: List<TaskSetITR>) {
    val gsonWriter = getGsonWriter()
    for (taskSet in taskSets) {
        val modifiedTaskSet = taskSet.cloneBesidesMaps(namespaceCode = namespace)

        val writer = FileWriter(outputDirectoryPath + taskSet.code + ".json")
        gsonWriter.toJson(modifiedTaskSet, writer)
        writer.flush()
        writer.close()
    }
}


fun printJsonTaskSetsToConsole(namespace: String, taskSets: List<TaskSetITR>) {
    val gsonWriter = getGsonWriter()
    for (taskSet in taskSets) {
        val modifiedTaskSet = taskSet.cloneBesidesMaps(namespaceCode = namespace).toTaskSetFrontInput()

        val writer = FileWriter(FileDescriptor.out)
        gsonWriter.toJson(modifiedTaskSet, writer)
        writer.flush()
        writer.close()
    }
}
