package mathhelper.seambl.benchmarking

import mathhelper.seambl.api.compareWithoutSubstitutions
import mathhelper.seambl.api.stringToExpression
import mathhelper.seambl.baseoperations.BaseOperationsDefinitions
import mathhelper.seambl.config.ComparisonType
import mathhelper.seambl.config.CompiledConfiguration
import mathhelper.seambl.expressiontree.ExpressionNodeConstructor
import kotlin.test.Ignore
import kotlin.test.Test
import mathhelper.seambl.substitutiontests.parseStringExpression
import kotlin.test.assertEquals

fun stringToExpresionShortest (){
val a = "hjgh"+"hjgk"
    val b = a + a
//    stringToExpression("A", scope = "setTheory")
}

fun stringToExpresionShort (){
    stringToExpression("(!!C&!B)|A", scope = "setTheory")
}

fun stringToExpresionMiddle (){
    stringToExpression("(!!C&!B)|A->(!!C&!B)|A", scope = "setTheory")
}



class ExpressionParserMeasure {
    @Test
    @Ignore
    fun stringToExpressionMeasureTest() {
        makeMeasures(listOf(
                Pair("shortest", { stringToExpresionShortest() }),
                Pair("short", { stringToExpresionShort() }),
                Pair("middle", { stringToExpresionMiddle() })
        ), 10)
    }
}
