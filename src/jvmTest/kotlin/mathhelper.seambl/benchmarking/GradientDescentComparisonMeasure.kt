package mathhelper.seambl.benchmarking

import mathhelper.seambl.baseoperations.BaseOperationsDefinitions
import mathhelper.seambl.config.ComparisonType
import mathhelper.seambl.config.CompiledConfiguration
import mathhelper.seambl.expressiontree.ExpressionNodeConstructor
import mathhelper.seambl.optimizerutils.OptimizerUtils
import kotlin.test.Ignore
import kotlin.test.Test
import mathhelper.seambl.platformdependent.random
import mathhelper.seambl.substitutiontests.parseStringExpression
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.test.assertEquals

class GradientDescentComparisonMeasure {
    val compiledConfiguration = CompiledConfiguration()

    val left = "(sin(a) + cos(b) + exp(c + sin(d + e) + cos(f * g - a)))^8*100"
    val right = "-1"
    val expression = "($left)-($right)"

    fun simpleCompare (){
        val minizer = OptimizerUtils(parseStringExpression(expression), compiledConfiguration = compiledConfiguration)
        if (minizer.canStart())
            minizer.run(4)
    }


    @Test
    @Ignore
    fun stringToExpressionMeasureTest() {
        makeMeasures(listOf(
                Pair("simple", { simpleCompare() })
        ), 100)
    }
}