plugins {
    kotlin("multiplatform") version "1.5.10"
    id("maven-publish")
}

group = "mathhelper.seambl"
version = "1.0.0"

repositories {
    mavenCentral()
}

kotlin {
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
    }
    js() {
        nodejs {
        }
        binaries.executable()
    }

    
    sourceSets {
        val commonMain by getting{
            dependencies {
                implementation("com.google.code.gson:gson:2.8.8")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
        val jvmMain by getting
        val jvmTest by getting
        val jsMain by getting
        val jsTest by getting
    }
}
